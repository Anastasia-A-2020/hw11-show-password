const refs = {
  form : document.forms[0],
  icons: document.querySelectorAll(".icon-password"),
  inputs: document.querySelectorAll("input"),
  describeWarning: document.querySelector(".warning"),
}

refs.form.addEventListener("click", onIconClick);
refs.form.addEventListener("click", onButtonSubmit);

function onIconClick(e) {
  e.preventDefault();

  if (e.target.tagName === "I") {
    e.target.classList.toggle("fa-eye-slash");

    if (e.target.classList.contains("fa-eye-slash")) {
      e.target.previousElementSibling.type = "password";
    } else {
      e.target.previousElementSibling.type = "text";
    };
  }
}

function onButtonSubmit(e) {
  e.preventDefault();  

  if (e.target.tagName === "BUTTON") {
    const inputValues = [];
    for (const el of refs.inputs) {
      inputValues.push(el.value)
    }
    if (inputValues[0] === inputValues[1]) {
      refs.describeWarning.textContent = "";
      alert("You are welcome");      
    } else {
      refs.describeWarning.textContent = "Потрібно ввести однакові значення";
    }
  };
  
}
